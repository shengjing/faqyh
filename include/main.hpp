/**
 * @file main.hpp
 * @brief 主程序头文件
 * @author DSOE1024
 */
#pragma once

#ifndef MAIN_HPP
#define MAIN_HPP

#include "iostream"
#include "limits"
#include "filesystem"
#include "csignal"
#include "logger.hpp"
#include "hwinfo.hpp"
#include "terminalscreen.hpp"
#include "formatter.hpp"

#endif